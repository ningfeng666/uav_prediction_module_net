FROM zerocm/zcm
MAINTAINER littleHuang<ysh626.std@gmail.com>
RUN apt install -y g++ make cmake libboost-all-dev libeigen3-dev libjsoncpp-dev
ENV SRC_DIR /prediction_module
ADD prediction_module_net.tar.gz $SRC_DIR
ENV LD_LIBRARY_PATH /usr/lib:/usr/local/lib
ENV ZCM_DEFAULT_URL udpm://239.255.76.67:7667?ttl=1
WORKDIR $SRC_DIR/prediction_module_code/src/prediction
RUN mkdir build&&cd build&& cmake ..&&make
EXPOSE 7667
CMD cd build&&./prediction
